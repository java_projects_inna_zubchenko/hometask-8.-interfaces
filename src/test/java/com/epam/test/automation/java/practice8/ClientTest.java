package com.epam.test.automation.java.practice8;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.testng.Assert.assertTrue;

public class ClientTest {
    @DataProvider(name = "addDepositTest")
    public Object[][] dataProviderMethod() {
        return new Object[][]{{new Deposit[]{new LongDeposit(new BigDecimal(1100), 36), new SpecialDeposit(new BigDecimal(1000), 2), new BaseDeposit(new BigDecimal(1000), 1)}, true}, {new Deposit[]{new LongDeposit(new BigDecimal(1100), 36), new SpecialDeposit(new BigDecimal(1000), 2), new BaseDeposit(new BigDecimal(1000), 1),
                new LongDeposit(new BigDecimal(2001), 10), new SpecialDeposit(new BigDecimal(500), 2), new BaseDeposit(new BigDecimal(5000), 21),
                new LongDeposit(new BigDecimal(700), 34), new SpecialDeposit(new BigDecimal(800), 15), new BaseDeposit(new BigDecimal(300), 10), new BaseDeposit(new BigDecimal(1000), 6)}, false}};
    }
    @Test(dataProvider ="addDepositTest")
    public void testAddDeposit(Deposit[] deposits, boolean expected){
        Client client = new Client();
        for (Deposit deposit: deposits){
            client.addDeposit(deposit);
        }
        var actual = client.addDeposit(new BaseDeposit(new BigDecimal(300), 10));
        Assert.assertEquals(actual, expected, "The method addDeposit works incorrect");
    }

    @DataProvider(name = "totalIncomeTest")
    public Object[][] dataProviderMethod1(){
        return new Object[][]{{new Deposit[]{new LongDeposit(new BigDecimal(1001), 34), new SpecialDeposit(new BigDecimal(500), 2), new BaseDeposit(new BigDecimal(1000), 1)}, BigDecimal.valueOf(49179.78)}};
    }
    @Test(dataProvider = "totalIncomeTest")
    public void testTotalIncome(Deposit[] deposits, BigDecimal expected){
        Client client = new Client();
        for (Deposit deposit: deposits) {
            client.addDeposit(deposit);
        }
        var actual = client.totalIncome();
        Assert.assertEquals(actual, expected, "The method totalIncome works incorrectly");
    }


    @DataProvider(name = "maxIncomeTest")
    public Object[][] dataProviderMethod2(){
        return new Object[][]{{new Deposit[]{new LongDeposit(new BigDecimal(1000),7), new SpecialDeposit(new BigDecimal(1000), 2), new BaseDeposit(new BigDecimal(1000), 1)}, new BigDecimal("150.00")}};
    }
    @Test(dataProvider ="maxIncomeTest")
    public void testMaxIncomeMethod(Deposit[] deposits,BigDecimal expected){
        Client client = new Client();
        for (Deposit deposit: deposits) {
            client.addDeposit(deposit);
        }
        var actual = client.maxIncome();
        Assert.assertEquals(actual, expected, "The method addDeposit works incorrect");
    }

    @DataProvider(name = "countPossibleToProlongTest")
    public Object[][] dataProviderMethod3() {
        return new Object[][]{{new Deposit[]{new LongDeposit(new BigDecimal(1001), 34), new SpecialDeposit(new BigDecimal(500), 2), new BaseDeposit(new BigDecimal(1000), 1)}, 1}};
    }
    @Test(dataProvider = "countPossibleToProlongTest")
    public void testCountToProlong(Deposit[] deposits, int expected){
        Client client = new Client();
        for (Deposit deposit: deposits) {
            client.addDeposit(deposit);
        }
        var actual = client.countPossibleToProlongDeposit();
        Assert.assertEquals(actual, expected, "The method countPossibleToProlongDeposit works incorrectly");
    }

    @DataProvider(name = "getIncomeByNumberTest")
    public Object[][] dataProviderMethod4() {
        return new Object[][]{{new Deposit[]{new LongDeposit(new BigDecimal(1001), 36), new SpecialDeposit(new BigDecimal(1000), 2), new BaseDeposit(new BigDecimal(1000), 1)}, new BigDecimal("50.00")}};
    }
    @Test(dataProvider = "getIncomeByNumberTest")
    public void testIncomeByNumber(Deposit[] deposits, BigDecimal expected){
        Client client = new Client();
        for (Deposit deposit: deposits) {
            client.addDeposit(deposit);
        }
        var actual = client.getIncomeByNumber(2);
        Assert.assertEquals(actual, expected);
    }

    @DataProvider(name = "sortDepositTest")
    public Object[][] dataProviderMethod5() {
            return new Object[][]{{new Deposit[]{new LongDeposit(new BigDecimal(10000), 10), new SpecialDeposit(new BigDecimal(500), 2), new BaseDeposit(new BigDecimal(5000), 21)},
                    new Deposit[]{ new LongDeposit(new BigDecimal(10000), 10), new BaseDeposit(new BigDecimal(5000), 21), new SpecialDeposit(new BigDecimal(500), 2), null, null, null, null, null, null, null}}};
    }

    @Test(dataProvider = "sortDepositTest")
    public void testSortDeposit(Deposit[] deposits, Deposit[] expected){
        Client client = new Client();
        for (Deposit deposit: deposits) {
            client.addDeposit(deposit);
            client.sortDeposits();
        }
        var actual = client.getArrayDeposits();
        assertTrue(Arrays.equals(actual, expected));
        }

}
