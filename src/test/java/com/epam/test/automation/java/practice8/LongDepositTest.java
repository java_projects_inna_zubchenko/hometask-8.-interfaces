package com.epam.test.automation.java.practice8;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.math.BigDecimal;

public class LongDepositTest {
    @DataProvider(name = "incomeTest")
    public Object[][] dataProviderMethod(){
        return new Object[][]{{new LongDeposit(new BigDecimal(1000),7), BigDecimal.valueOf(1000).multiply(BigDecimal.valueOf(0.15))},
        {new LongDeposit(new BigDecimal(1000),3), BigDecimal.valueOf(0)}};
    }
    @Test(dataProvider ="incomeTest")
    public void testIncomeMethod(LongDeposit longDeposit, BigDecimal expected){
        var actual = longDeposit.income();
        Assert.assertEquals(actual, expected, "The method income works incorrect");
    }
}