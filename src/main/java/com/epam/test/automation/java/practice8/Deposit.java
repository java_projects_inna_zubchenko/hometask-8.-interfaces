package com.epam.test.automation.java.practice8;
import java.math.BigDecimal;

public abstract class Deposit implements Comparable<Deposit>{
    public final BigDecimal amount;
    public final int period;

    protected Deposit(BigDecimal amount, int period) {
        this.amount = amount;
        this.period = period;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public int getPeriod() {
        return period;
    }

    public abstract BigDecimal income();

    @Override
    public int compareTo(Deposit other){
        if (this.amount == null || this.income() == null) {
            return (other == null) ? 0 : 1;
        }
        if (other == null) {
            return -1;
        }
        BigDecimal totalSum = this.amount.add(this.income());
        BigDecimal otherTotalSum = other.amount.add(other.income());
        return totalSum.compareTo(otherTotalSum);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj != null && getClass() == obj.getClass()) {
            Deposit deposit = (Deposit) obj;
            return this.amount.add(income()).equals(deposit.amount.add(deposit.income()));
        } else {
            return false;
        }
    }

    @Override
    public int hashCode(){
        return this.getAmount().add(income()).intValue();
    }

}
