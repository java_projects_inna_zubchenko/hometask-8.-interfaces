package com.epam.test.automation.java.practice8;
import java.math.BigDecimal;
import java.util.*;
import java.util.function.Consumer;

public class Client implements Iterator<Deposit>, Iterable<Deposit>{
    private final Deposit[] deposits;
    public Client(){
        deposits = new Deposit[10];
    }
    public Deposit[] getArrayDeposits(){
        return deposits;
    }
    public boolean addDeposit(Deposit deposit) {
        for (int i = 0; i < deposits.length; i++) {
            if (deposits[i] == null) {
                deposits[i] = deposit;
                return true;
            }
        }
        return false;
    }
    public BigDecimal totalIncome()
    {
        BigDecimal totalAmount = BigDecimal.valueOf(0);
        for (Deposit deposit : deposits) {
            if (deposit != null){
                totalAmount = totalAmount.add(deposit.income());
            }
        }
        return totalAmount;
    }
    public BigDecimal maxIncome()
    {
        BigDecimal maxIncome = BigDecimal.valueOf(0);
        for(Deposit deposit: deposits)
        {
            if (deposit == null)
                break;
            if (maxIncome.compareTo(deposit.income())<0)
                maxIncome = deposit.income();
        }
        return maxIncome;
    }
    public BigDecimal getIncomeByNumber(int n)
    {
        BigDecimal income = BigDecimal.valueOf(0);
        if (deposits[n] != null) {
            income = deposits[n].income();
        }
        return income;
    }
    public void sortDeposits(){
        if(deposits != null) {
            Arrays.sort(deposits,
                    Comparator.nullsLast(
                            Comparator.reverseOrder()));
        }
    }

    public int countPossibleToProlongDeposit()
    {
        int count = 0;
        for (Deposit deposit: deposits)
        {
            if (deposit instanceof Prolongable && ((Prolongable) deposit).canToProlong()) {
                count++;
            }
        }
        return count;
    }

    @Override
    public boolean hasNext() {
        return Arrays.stream(deposits).iterator().hasNext();
    }

    @Override
    public Deposit next() {
        if (!hasNext()){
            throw new NoSuchElementException();
        }
        return Arrays.stream(deposits).iterator().next();
    }

    @Override
    public Iterator<Deposit> iterator() {
        return Arrays.stream(this.deposits).iterator();
    }

    @Override
    public void forEach(Consumer<? super Deposit> action) {
        Objects.requireNonNull(action);
        for (Deposit deposit : this) {
            action.accept(deposit);
        }
    }
}
